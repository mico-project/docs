![MICO](http://www.mico-project.eu/wp-content/uploads/2014/04/mico_logo.png)



#  The Mico Project - Media in Context 


MICO is shorthand for “Media in Context”, a research project partially funded by the European Commission 7th Framework Programme, running from ... to .....

The main goal of the project is to provide meaning to cross-media content in the form of automatic annotations i.e. machine-readable code, in which as much of the context of the content is annotated as possible, regardless of whether the annotations come from analysing the text, audio, image or video elements of the composite cross-media content.

We developed a platform.....

## [Platform Overview](sections/overview/README.md)


----------


## [Quickstart: Installing a ready to use MICO platform](sections/quickstart/README.md)

## [Using the MICO platform](sections/basic-usage/README.md)

## [Creating and managing workflows](sections/workflow-creation/README.md)

## [Advanced MICO platform administration](sections/advanced-usage/README.md)

## [Developing new extractors for the MICO platform](sections/extractor-dev/README.md)


----------


## [API reference](sections/API/README.md)